import './../css/main.css';
import './ux.js';
import { Greeter } from './typescript.ts';
import ima1 from "./images/01.jpg";
import ima2 from "./images/02.jpg";
import ima3 from "./images/03.jpg";

var imag1 = document.getElementById('ima1');
var imag2 = document.getElementById('ima2');
var imag3 = document.getElementById('ima3');
imag1.src = ima1;
imag2.src = ima2;
imag3.src = ima3;

var cont = document.querySelector(".container");
cont.appendChild(imag1);
cont.appendChild(imag2);
cont.appendChild(imag3);


var g = new Greeter("Tenemos typescript");
console.log(g.greet());

